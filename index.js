const puppeteer = require('puppeteer');
const fs = require("fs");

const link ={
    init: "http://edusoftweb.hcmiu.edu.vn/default.aspx",
    register: "http://edusoftweb.hcmiu.edu.vn/Default.aspx?page=dkmonhoc",
    ajaxpro: 'http://edusoftweb.hcmiu.edu.vn/ajaxpro/EduSoft.Web.UC.DangKyMonHoc,EduSoft.Web.ashx',
};

(async () => {
    const start = new Date().getTime();

    // Choose the data of user
    const data = JSON.parse(fs.readFileSync('db.json','utf8'))[0];
    const { account, requests } = data;

    // const browser = await puppeteer.launch({ headless: false });
    const browser = await puppeteer.launch();
    console.log('Browser opened');
    const page = await browser.newPage();
    page.setViewport({ width: 1280, height:720 });
    await page.goto(link.init);
    console.log("Page loaded");

    const enterCode = await page.evaluate(() => {
        return document.getElementById("ctl00_ContentPlaceHolder1_ctl00_lblCapcha").innerText;
    })
    console.log(`Code: ${enterCode}`);
    await page.type('#ctl00_ContentPlaceHolder1_ctl00_txtCaptcha', enterCode);
    await page.click('#ctl00_ContentPlaceHolder1_ctl00_btnXacNhan');

    await page.waitForNavigation();

    const enterCookies = await page.cookies();
    // console.log(enterCookies);


    await page.type("#ctl00_ContentPlaceHolder1_ctl00_ucDangNhap_txtTaiKhoa", account.username);
    await page.type("#ctl00_ContentPlaceHolder1_ctl00_ucDangNhap_txtMatKhau", account.password);

    await page.click("#ctl00_ContentPlaceHolder1_ctl00_ucDangNhap_btnDangNhap");

    await page.waitForNavigation();

    const loginCookies = await page.cookies();
    // console.log(loginCookies);
    console.log("Logged in.");
    const end1 = new Date().getTime();

    // Ver 1
    // Using Click:
    // await page.goto("http://edusoftweb.hcmiu.edu.vn/Default.aspx?page=dkmonhoc");

    // await Promise.all(subjects.map(async (subject) => {
    //     const page = await browser.newPage();
    //     await page.goto(link.register);
    //     console.log("Open link for "+ subject.name);
    //     await page.type("#txtMaMH1", subject.id);
    //     await page.click("#btnLocTheoMaMH1");
    //     // console.log(`Wait for [id="${subject.btnId}"]`)
    //     await page.waitForSelector(`[id="${subject.btnId}"]`)
    //     // await console.log("Appeared");
    //     // await page.click(`[id="${subject.btnId}"]`);
    //     // await page.click("#btnLuu");
    //     // await page.waitForNavigation();
    //     console.log(`Selected ${subject.name}`);
    // }));
    // console.log("Load Done");
    // await page.goto("http://edusoftweb.hcmiu.edu.vn/Default.aspx?page=dkmonhoc");
    // await page.click("#btnLuu");
    // await page.waitForNavigation();
    // console.log("Finish registering courses.");


    // Ver 2
    // Send AJAX to server
    await page.goto("http://edusoftweb.hcmiu.edu.vn/Default.aspx?page=dkmonhoc");

    await Promise.all(requests.map(async (request)=> {
        const page = await browser.newPage();
        await page.setRequestInterception(true);

        // Request intercept handler... will be triggered with
        // each page.goto() statement
        page.on('request', interceptedRequest => {

            // Here, is where you change the request method and
            // add your post data
            var data = {
                'method': 'POST',
                'postData': JSON.stringify(request)
            };

            // Request modified... finish sending!
            interceptedRequest.continue(data);
        });

        // Navigate, trigger the intercept, and resolve the response
        await page.goto('http://edusoftweb.hcmiu.edu.vn/ajaxpro/EduSoft.Web.UC.DangKyMonHoc,EduSoft.Web.ashx');

        console.log(`Finish ${request.tenMH}.`);
        await page.close();
    }));

    await page.goto("http://edusoftweb.hcmiu.edu.vn/Default.aspx?page=dkmonhoc");

    console.log("\nFinish all courses.");

    console.log("Thank you for using this tool.");

    const end2 = new Date().getTime();
    const time1 = end1 - start; //Login time
    const time2 = end2 - end1; //Register time
    const time  = end2 - start;
    console.log("Login time: " + time1);
    console.log("Register time: "+ time2);
    console.log('Execution time: ' + time);

    await browser.close();
})()
