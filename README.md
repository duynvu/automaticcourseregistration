# Automatic Course Registration
This tool is used for automatic courses registration for my university.
It is just do the process more quickly with fewer amount of requests.

## [Internation University - HCMIU](http://edusoftweb.hcmiu.edu.vn/default.aspx)


### Nguyen Vu Duy - ITITIU16006

### Tech Stack: 
* NodeJs
* PuppeteerJS

## How to use:
* Open Terminal and go to the directory.
* For the first time, run this to install package and generate data:
    >npm i
    >node input.js
* To run:
    node index.js

## Note:
* Data is the real information of 2 students. It is valid until 24:00 24/12/2018.
* I have not handle error yet.

## Commands:
* "node read.js" to read the data.
* You can change the information of student in input.js and run "node input.js" to change the data.