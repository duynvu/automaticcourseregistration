const fs = require('fs');

let subjects = [
    {
        maDK: "PE008IU        04",
        maMH: "PE008IU",
        maNh: "04",
        sotc: "3",
        strsoTCHP: "3.0",
        tenMH: "Critical Thinking"
    },
    {
        maDK: "IT089IU        02    02",
        maMH: "IT089IU",
        maNh: "02",
        sotc: "4",
        strsoTCHP: "4.0",
        tenMH: "Computer Architecture"
    },
    {
        maDK:"IT079IU        02    01",
        maMH: "IT079IU",
        maNh: "02",
        sotc: "4",
        strsoTCHP: "4.0",
        tenMH: "Principles of Database Management"
    },
    {
        maDK: "PH013IU        03",
        maMH: "PH013IU",
        maNh: "03",
        sotc: "2",
        strsoTCHP: "2.0",
        tenMH: "Physics 1"

    }
];

const data = [
    {
        account: {
            username: "ITITIU16006",
            password: "vuduy1998",        
        },
        requests: [
            {
                isCheck: "true",
                isMHDangKyCungKhoiSV: "0",
                isValidCoso: false,
                isValidTKB: false,
                maDK: "IT093IU        01    01",
                maMH: "IT093IU",
                maNh: "01",
                oldMaDK: "",
                soTiet: "0",
                sotc: "4",
                strngayThi: "01/01/0001",
                strsoTCHP: "4.0",
                tenMH: "Web Application Development",
                tietBD: "0",
            },
            {
                isCheck: "true",
                isMHDangKyCungKhoiSV: "0",
                isValidCoso: false,
                isValidTKB: false,
                maDK: "PE012IU        04",
                maMH: "PE012IU",
                maNh: "04",
                oldMaDK: "",
                soTiet: "0",
                sotc: "2",
                strngayThi: "01/01/0001",
                strsoTCHP: "2.0",
                tenMH: "Ho Chi Minh's Thoughts",
                tietBD: "0"
            },
            {
                isCheck: "true",
                isMHDangKyCungKhoiSV: "0",
                isValidCoso: false,
                isValidTKB: false,
                maDK: "PE013IU        02",
                maMH: "PE013IU",
                maNh: "02",
                oldMaDK: "",
                soTiet: "0",
                sotc: "3",
                strngayThi: "01/01/0001",
                strsoTCHP: "3.0",
                tenMH: "Revolutionary Lines of Vietnamese Communist Party",
                tietBD: "0"
            },
            {
                isCheck: "false",
                isMHDangKyCungKhoiSV: "0",
                isValidCoso: false,
                isValidTKB: false,
                maDK: "IT013IU        02    01",
                maMH: "IT013IU",
                maNh: "02",
                oldMaDK: "",
                soTiet: "0",
                sotc: "4",
                strngayThi: "01/01/0001",
                strsoTCHP: "4.0",
                tenMH: "Algorithms & Data Structures",
                tietBD: "0"
            }
        ],
    },
    {
        account: {
            username: "ITITUN17022",
            password: "141099",
        },
        requests: subjects.map(convert),
    },
];


(async () => {
    const json = JSON.stringify(data);
    await fs.writeFile('db.json', json, 'utf8', ()=> {});
    console.log("Writed.")
})()

function convert(course) {
    return {
        isCheck: "true",
        isMHDangKyCungKhoiSV: "0",
        isValidCoso: false,
        isValidTKB: false,
        maDK: course.maDK,
        maMH: course.maMH,
        maNh: course.maNh,
        oldMaDK: "",
        soTiet: "0",
        sotc: course.sotc,
        strngayThi: "01/01/0001",
        strsoTCHP: course.strsoTCHP,
        tenMH: course.tenMH,
        tietBD: "0",
     }
}

// console.log(subjects)

// 1 . Critical Thinking
// Mã môn học : PE008IU
// Mã Lớp : CECE17IU21
// Thứ 3 tiết 4 ,   
// Thầy Khôi . 

// 2 . Computer Architecture
// Mã môn học : IT089IU
// Mã lớp : ITIT17IU31
// Thứ 2 tiết 4 , thứ 6 tiết 1 ; 
// Thầy Dương ; 

// 3.  Principles of Database Management
// Mã môn học : IT079IU
// Mã Lớp : ITIT17UN21	
// Thứ 4 tiết 4 , thứ 7 tiết 1 ;
// Cô Loan

// 4 . Physics 1
// Mã môn học: PH013IU
// Mã lớp : EEEE18RG21
// Thứ 2 tiết 1 ;
// Cô Tâm

// User : ITITUN17022
// Pass : 141099
